﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class HistoryPasienViewModel
    {
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string NoIdentitas { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string TglLahir_View { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
    }
}