﻿using iHos.MVC.Property;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class APIMasterServices
    {
        public class ReferensiSectionModel
        {
            public List<Section> Data { get; set; }
            public int TotalCount  { get; set; }
            public bool Success { get; set; }
            public string Message { get; set; }
        }

        public class GetSectionModel
        {
            public Section Data { get; set; }
            public int TotalCount { get; set; }
            public bool Success { get; set; }
            public string Message { get; set; }
        } 
        public class Section
        {
            public string Id { get; set; }
            public string Nama { get; set; }
        }

        public class ReferensiRegistrasiModel
        {
            public List<Registrasi> Data { get; set; }
            public int TotalCount { get; set; }
            public bool Success { get; set; }
            public string Message { get; set; }
        }

        public class GetRegistrasiModel
        {
            public Registrasi Data { get; set; }
            public int TotalCount { get; set; }
            public bool Success { get; set; }
            public string Message { get; set; }
        }
        public class Registrasi
        {
            public string NoReg { get; set; }
            public string NRM { get; set; }
            public string NamaPasien { get; set; }
            public string NoKamar { get; set; }
        }

        public class ReferensiDokterModel
        {
            public List<Dokter> Data { get; set; }
            public int TotalCount { get; set; }
            public bool Success { get; set; }
            public string Message { get; set; }
        }

        public class GetDokterModel
        {
            public Section Data { get; set; }
            public int TotalCount { get; set; }
            public bool Success { get; set; }
            public string Message { get; set; }
        }
        public class Dokter
        {
            public string Id { get; set; }
            public string Nama { get; set; }
        }
        protected string BaseUrl = ConfigurationManager.AppSettings["BaseUrl"];

        public ReferensiSectionModel ReferensiSection(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string filter_id, string filter_nama)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"api/Section?sortBy={sortBy}&sortByType={sortByType}&pageSize={pageSize}&pageIndex={pageIndex}&filter_id={filter_id}&filter_nama={filter_nama}", Method.GET);
            var response = client.Execute<ReferensiSectionModel>(request);
            return response.Data;
        }

        public ReferensiRegistrasiModel ReferensiRegistrasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string filter_noreg, string filter_nrm, string filter_namapasien)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"api/Registrasi?sortBy={sortBy}&sortByType={sortByType}&pageSize={pageSize}&pageIndex={pageIndex}&filter_noreg={filter_noreg}&filter_nrm={filter_nrm}&filter_namapasien={filter_namapasien}", Method.GET);
            var response = client.Execute<ReferensiRegistrasiModel>(request);
            return response.Data;
        }

        public ReferensiDokterModel ReferensiDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string filter_id, string filter_nama)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"api/Dokter?sortBy={sortBy}&sortByType={sortByType}&pageSize={pageSize}&pageIndex={pageIndex}&filter_id={filter_id}&filter_nama={filter_nama}", Method.GET);
            var response = client.Execute<ReferensiDokterModel>(request);
            return response.Data;
        }

        public GetSectionModel GetSection(string id)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"api/Section/{id}", Method.GET);
            var response = client.Execute<GetSectionModel>(request);
            return response.Data;
        }

        public GetRegistrasiModel GetRegistrasi(string id)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"api/Registrasi/{id}", Method.GET);
            var response = client.Execute<GetRegistrasiModel>(request);
            return response.Data;
        }

        public GetDokterModel GetDokter(string id)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"api/Dokter/{id}", Method.GET);
            var response = client.Execute<GetDokterModel>(request);
            return response.Data;
        }
    }
}