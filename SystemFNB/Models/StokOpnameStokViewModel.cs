﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class StokOpnameViewModel
    {
        public string No_Bukti { get; set; }
        public DateTime Tgl_Opname { get; set; }
        public string Tgl_Opname_View { get; set; }
        public string Lokasi { get; set; }
        public ListDetail<StokOpnameDetailViewModel> Detail_List { get; set; }
        public List<StokOpnameDetailViewModel> Detail_List2 { get; set; }
        public string KelompokJenis { get; set; }
        public string Keterangan { get; set; }
        public bool Posted { get; set; }
    }

    public class StokOpnameDetailViewModel
    {
        public bool Remove { get; set; }
        public string No_Bukti { get; set; }
        public int Barang_ID { get; set; }
        public string Kode_Barang  { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public string Konversi{ get; set; }
        public float QtySystem { get; set; }
        public float QtyFisik { get; set; }
        public float QtySelisih { get; set; }
        public string Harga { get; set; }
        public string Kategori { get; set; }
        public string Keterangan { get; set; }
    }
}