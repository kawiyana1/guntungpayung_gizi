﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class FNB_mProdukViewModel
    {
        public string Kode { get; set; }
        public string Nama { get; set; }
        public int Kode_Kategori { get; set; }
        public int Kode_Kelompok { get; set; }
        public bool Diet { get; set; }
        public bool Aktif { get; set; }
    }
}