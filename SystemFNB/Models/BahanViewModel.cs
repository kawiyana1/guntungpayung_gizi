﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class BahanViewModel
    {
        public string Kode { get; set; }
        public int Kode_Satuan { get; set; }
        public string NamaSatuan { get; set; }
        public string Nama { get; set; }
        public bool Aktif { get; set; }
    }
}