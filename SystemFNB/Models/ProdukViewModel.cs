﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class ProdukViewModel
    {
        public string Kode { get; set; }
        public string Nama { get; set; }
        public int Kode_Kategori { get; set; }
        public string NamaKategori { get; set; }
        public int Kode_Kelompok { get; set; }
        public string NamaKelompok { get; set; }
        public Nullable<decimal> Harga { get; set; }
        public string Harga_View { get; set; }
        public bool Diet { get; set; }
        public bool Aktif { get; set; }
        public ListDetail<ProdukDetailViewModel> Detail_List { get; set; }
    }

    public class ProdukDetailViewModel
    {
        public string Kode_Produk { get; set; }
        public string Kode_Bahan { get; set; }
        public string NamaBahan { get; set; }
        public double Qty { get; set; }
    }

    public class testing
    {
        public string Kode_Produk { get; set; }
        public string Qty { get; set; }
    }
}