﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class DetailHistoryPasienViewModel
    {
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string NoIdentitas { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string TglLahir_View { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public System.DateTime TanggalPenyajian { get; set; }
        public string TanggalPenyajian_View { get; set; }
        public string NamaShift { get; set; }
        public string NamaMakanan { get; set; }
        public string NamaDiet { get; set; }
    }
}