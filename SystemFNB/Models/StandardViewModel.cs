﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class StandardViewModel
    {
        public string Kode { get; set; }
        public string Nama { get; set; }
    }
}