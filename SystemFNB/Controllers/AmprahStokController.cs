﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SystemFNB.Entities;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace SystemFNB.Controllers
{
    [Authorize(Roles = "FnB")]
    public class AmprahStokController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            var section_id = Request.Cookies["SectionIDGizi"].Value;
            var section_name = Request.Cookies["SectionNameGizi"].Value;
            var item = new AmprahStokViewModel();

            if(section_id == null)
            {
                using (var sim = new SIMEntities())
                {
                    var sec = sim.SIMmSection.FirstOrDefault(x => x.KelompokSection == "FB & CAFETARIA");
                    if(sec != null)
                    {
                        item.SectionAsalID = sec.SectionID;
                        item.SectionAsalName = sec.SectionName;
                    }
                }
            }

            item.Tanggal = DateTime.Today;
            item.SectionAsalID = section_id;
            item.SectionAsalName= section_name;

            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new AmprahStokViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Validation
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<AmprahStokDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);
                                if (item.Detail_List.Count == 0)
                                {
                                    throw new Exception("Detail Tidak Boleh Kosong");
                                }
                                foreach(var x in item.Detail_List)
                                {
                                    var exist = item.Detail_List.Where(e => e.Model.Kode_Barang == x.Model.Kode_Barang);
                                    if (exist.Count() > 1)
                                    {
                                        var _exist = exist.FirstOrDefault();
                                        throw new Exception(_exist.Model.NamaBarang + ", telah dimasukan lebih dari satu");
                                    }
                                }
                                #endregion

                                //var section = Request.Cookies["SectionIDGizi"].Value;
                                var id = s.AutoNumber_Pelayanan_GD_trAmprahan().FirstOrDefault();
                                var m = new GD_trAmprahan()
                                {
                                    NoBukti = id,
                                    Tanggal = item.Tanggal,
                                    SectionAsal = item.SectionAsalID,
                                    SectionTujuan = item.SectionTujuan,
                                    UserID = 1103,
                                    Disetujui = false,
                                    Keterangan = item.Keterangan,
                                    JamUpdate = DateTime.Now,
                                    HostName = "WEB"
                                };
                                s.GD_trAmprahan.Add(m);

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    x.Model.NoBukti = m.NoBukti;
                                    var b = s.GetDataObatUmum(item.SectionTujuan).FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                                    x.Model.Satuan = b.Satuan;
                                }
                                var d = item.Detail_List.ConvertAll(x => IConverter.Cast<GD_trAmprahanDetail>(x.Model)).ToArray();
                                foreach (var x in d) { s.GD_trAmprahanDetail.Add(x); }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                //{
                                //    Activity = $"AmprahStok Cerate {m.NoBukti}"
                                //};
                                //UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string id)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            var m = s.GD_trAmprahan.FirstOrDefault(x => x.NoBukti == id);
                            if (m == null) throw new Exception("Data Tidak ditemukan");
                            if (m.Realisasi == false && m.Batal == false)
                            {
                                m.Batal = true;
                                result = new ResultSS(s.SaveChanges());
                                dbContextTransaction.Commit();
                            }
                            else
                            {
                                throw new Exception("Tidak diijinkan untuk di hapus");
                            }
                        }
                        catch (SqlException ex) { dbContextTransaction.Rollback(); return JsonHelper.JsonMsgError(ex); }
                        catch (Exception ex) { dbContextTransaction.Rollback(); return JsonHelper.JsonMsgError(ex); }
                    }

                    //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    //{
                    //    Activity = $"AmprahStok delete {id}"
                    //};
                    //UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            AmprahStokViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.GD_trAmprahan.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<AmprahStokViewModel>(m);
                    var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == item.SectionAsal);
                    item.SectionAsal = sectionasal.SectionName;
                    var sectiontujuan = s.SIMmSection.FirstOrDefault(x => x.SectionID == item.SectionTujuan);
                    item.SectionTujuan = sectiontujuan.SectionName;

                    var d = s.Pelayanan_ViewAmprahanStok.Where(x => x.NoBukti == id).ToList();
                    item.Detail_List = new ListDetail<AmprahStokDetailViewModel>();
                    foreach (var x in d)
                    {
                        var y = IConverter.Cast<AmprahStokDetailViewModel>(x);
                        y.Konversi = (int)(x.Konversi ?? 0);
                        y.Qty = x.QtyAmprahan;
                        y.NamaBarang = x.Nama_Barang;
                        y.Satuan = x.SatStok;
                        y.SatuanBeli = x.SatBeli;
                        item.Detail_List.Add(false, y);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== E D I T
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            AmprahStokViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.GD_trAmprahan.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<AmprahStokViewModel>(m);
                    var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == item.SectionAsal);
                    item.SectionAsal = sectionasal.SectionName;
                    var sectiontujuan = s.SIMmSection.FirstOrDefault(x => x.SectionID == item.SectionTujuan);
                    item.SectionTujuan = sectiontujuan.SectionID;
                    item.SectionTujuanName = sectiontujuan.SectionName;

                    var d = s.Pelayanan_ViewAmprahanStok.Where(x => x.NoBukti == id).ToList();
                    item.Detail_List = new ListDetail<AmprahStokDetailViewModel>();
                    foreach (var x in d)
                    {
                        var y = IConverter.Cast<AmprahStokDetailViewModel>(x);
                        y.Konversi = (int)(x.Konversi ?? 0);
                        y.Qty = x.QtyAmprahan;
                        y.NamaBarang = x.Nama_Barang;
                        y.Satuan = x.SatStok;
                        y.SatuanBeli = x.SatBeli;
                        item.Detail_List.Add(false, y);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new AmprahStokViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var model = s.GD_trAmprahan.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                                if (model == null) throw new Exception("Data Tidak ditemukan");

                                #region Validation
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<AmprahStokDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);
                                if (item.Detail_List.Count == 0) throw new Exception("Detail Tidak Boleh Kosong");
                                #endregion

                                model.Tanggal = item.Tanggal;
                                model.Keterangan = item.Keterangan;

                                #region Detail
                                var GD_trAmprahanDetail = s.GD_trAmprahanDetail.Where(x => x.NoBukti == item.NoBukti);
                                s.GD_trAmprahanDetail.RemoveRange(GD_trAmprahanDetail);

                                foreach (var x in item.Detail_List)
                                {
                                    x.Model.NoBukti = item.NoBukti;
                                    var b = s.GetDataObatUmum(item.SectionTujuan).FirstOrDefault(y => y.Barang_ID == x.Model.Barang_ID);
                                    x.Model.Satuan = b.Satuan;
                                }
                                var d = item.Detail_List.ConvertAll(x => IConverter.Cast<GD_trAmprahanDetail>(x.Model)).ToArray();
                                foreach (var x in d) { s.GD_trAmprahanDetail.Add(x); }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Amprahan Edit {item.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                if (ex.InnerException != null) ex = ex.InnerException;
                                if (ex.InnerException != null) ex = ex.InnerException;
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Pelayanan_ListAmprahan> proses = s.Pelayanan_ListAmprahan;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Pelayanan_ListAmprahan.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_ListAmprahan.SectionTujuan)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Pelayanan_ListAmprahan.Keterangan)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Pelayanan_ListAmprahan.Realisasi)}.Contains(@0)", filter[4]);
                    proses = proses.Where($"{nameof(Pelayanan_ListAmprahan.Batal)}=@0", false);
                    proses = proses.Where($"{nameof(Pelayanan_ListAmprahan.SectionAsalID)}=@0", Request.Cookies["SectionIDGizi"].Value);
                    if (filter[17] != "True" && DateTime.Parse(filter[15]) != null && DateTime.Parse(filter[16]) != null)
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[15]).Date);
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[16]).Date);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<AmprahStokViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDetail(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var section = (filter[8] == "")? "SEC031" : filter[8];
                   IQueryable<GetDataObatUmum_Pelayanan_Result> proses = s.GetDataObatUmum_Pelayanan(section);
                    if (IFilter.F_int(filter[0]) != null) proses = proses.Where($"{nameof(GetDataObatUmum_Pelayanan_Result.Barang_ID)}=@0", IFilter.F_int(filter[0]));
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(GetDataObatUmum_Pelayanan_Result.Nama_Barang)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(GetDataObatUmum_Pelayanan_Result.Satuan)}.Contains(@0)", filter[2]);
                    if (IFilter.F_Decimal(filter[3]) != null) proses = proses.Where($"{nameof(GetDataObatUmum_Pelayanan_Result.Stok)}=@0", IFilter.F_Decimal(filter[3]));
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(GetDataObatUmum_Pelayanan_Result.SubKategori)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(GetDataObatUmum_Pelayanan_Result.Kode_Barang)}.Contains(@0)", filter[5]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DataObatUmumViewModel>(x));
                    foreach (var x in m)
                    {
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}