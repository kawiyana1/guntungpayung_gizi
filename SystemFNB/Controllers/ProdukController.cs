﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using SystemFNB.Entities;

namespace SystemFNB.Controllers
{
    [Authorize(Roles = "FnB")]
    public class ProdukController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new ProdukViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Validation
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<ProdukDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);
                                //if (item.Detail_List.Count == 0)
                                //{
                                //    throw new Exception("Detail Tidak Boleh Kosong");
                                //}
                                #endregion

                                var m = IConverter.Cast<FNB_mProduk>(item);
                                m.Harga = item.Harga_View.ToDecimal();
                                s.FNB_mProduk.Add(m);

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    x.Model.Kode_Produk = m.Kode;
                                }
                                var d = item.Detail_List.ConvertAll(x => IConverter.Cast<FNB_mProdukDetail>(x.Model)).ToArray();
                                foreach (var x in d) { s.FNB_mProdukDetail.Add(x); }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                //{
                                //    Activity = $"FNB_mProduk Cerate {m.Kode}"
                                //};
                                //UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            ProdukViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.Vw_Produk.FirstOrDefault(x => x.Kode == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<ProdukViewModel>(m);
                    item.Harga_View = m.Harga.Value.ToMoney();
                    var d = s.Vw_ProdukDetail.Where(x => x.Kode_Produk == id).ToList();
                    item.Detail_List = new ListDetail<ProdukDetailViewModel>();
                    foreach (var x in d)
                    {
                        var y = IConverter.Cast<ProdukDetailViewModel>(x);
                        item.Detail_List.Add(false, y);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new ProdukViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var user = User.Identity.GetUserId();
                                #region Validation
                                var model = s.FNB_mProduk.FirstOrDefault(x => x.Kode == item.Kode);
                                if (model == null) throw new Exception("Data Tidak ditemukan");
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<ProdukDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);

                                //if (item.Detail_List.Count == 0) throw new Exception("Detail Tidak Boleh Kosong");

                                #endregion

                                #region Header
                                model.Nama = item.Nama;
                                model.Kode_Kategori = item.Kode_Kategori;
                                model.Kode_Kelompok = item.Kode_Kelompok;
                                model.Harga = item.Harga_View.ToDecimal();
                                model.Diet = item.Diet;
                                model.Aktif = item.Aktif;
                                #endregion

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    x.Model.Kode_Produk = item.Kode;
                                }
                                var new_list = item.Detail_List;
                                var real_list = s.FNB_mProdukDetail.Where(x => x.Kode_Produk == item.Kode).ToList();
                                // delete | delete where (real_list not_in new_list)
                                foreach (var x in real_list)
                                {
                                    var m = new_list.FirstOrDefault(y => y.Model.Kode_Bahan == x.Kode_Bahan);
                                    if (m == null) s.FNB_mProdukDetail.Remove(x);
                                }

                                foreach (var x in new_list)
                                {
                                    var _m = real_list.FirstOrDefault(y => y.Kode_Bahan == x.Model.Kode_Bahan);
                                    // add | add where (new_list not_in raal_list)
                                    if (_m == null)
                                    {
                                        s.FNB_mProdukDetail.Add(new FNB_mProdukDetail()
                                        {
                                            Kode_Produk = x.Model.Kode_Produk,
                                            Kode_Bahan = x.Model.Kode_Bahan,
                                            Qty = x.Model.Qty,
                                        });
                                    }
                                    // edit | where (new_list in raal_list)
                                    else
                                    {
                                        _m.Qty = x.Model.Qty;
                                    }
                                }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                if (ex.InnerException != null)
                                {
                                    if (ex.InnerException.Message == "Conflicting changes detected. This may happen when trying to insert multiple entities with the same key.")
                                    {
                                        throw new Exception("Detail tidak boleh sama");
                                    }
                                }
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string id)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.FNB_mProduk.FirstOrDefault(x => x.Kode == id);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    s.FNB_mProduk.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"FNB_mProduk delete {id}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoId_FNB_mProduk().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Vw_Produk> proses = s.Vw_Produk;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Vw_Produk.Kode)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Vw_Produk.Nama)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Vw_Produk.NamaKategori)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Vw_Produk.NamaKelompok)}.Contains(@0)", filter[3]);
                    if (filter[4] == "1")
                    {
                        proses = proses.Where(x => x.Diet == true);
                    }
                    else if (filter[4] == "2")
                    {
                        proses = proses.Where(x => x.Diet == false);
                    }
                    if (!string.IsNullOrEmpty(filter[5]))
                    {
                        string harga = filter[5];
                        proses = proses.Where(x => x.Harga.ToString().Contains(harga));
                    }
                    if (filter[6] == "1")
                    {
                        proses = proses.Where(x => x.Aktif == true);
                    }
                    else if (filter[6] == "2")
                    {
                        proses = proses.Where(x => x.Aktif == false);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ProdukViewModel>(x));
                    foreach (var x in m)
                    {
                        if (x.Harga != null)
                        {
                            x.Harga_View = String.Format("{0:C0}", Convert.ToInt32(x.Harga.Value));
                        }
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}