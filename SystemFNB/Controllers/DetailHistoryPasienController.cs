﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SystemFNB.Entities;
using SystemFNB.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SystemFNB.Controllers
{
    public class DetailHistoryPasienController : Controller
    {
        // GET: DetailHistoryPasien
        public ActionResult Index(string nrm)
        {
            var model = new RegistrasiPasienViewModel();
            if (nrm == null) return Redirect(Url.Action("Index", "HistoryPasien"));
            //var sectionid = Request.Cookies["EMRSectionIDPelayanan"].Value;
            using (var s = new SIMEntities())
            {
                var pasien = s.VW_DataPasienReg.FirstOrDefault(x => x.NRM == nrm);
                if (pasien == null) return Redirect(Url.Action("Index", "HistoryPasien"));
                model = IConverter.Cast<RegistrasiPasienViewModel>(pasien);
                var cekstatusbayar = s.VW_Registrasi.FirstOrDefault(x => x.NRM == nrm);
                var dokterdefault = s.mDokter.Where(xx => xx.DokterID == cekstatusbayar.DokterRawatID).FirstOrDefault();
                if (dokterdefault != null)
                {
                    model.DokterID = dokterdefault.DokterID;
                    model.NamaDOkter = dokterdefault.NamaDOkter;
                }
                model.StatusBayar = cekstatusbayar.StatusBayar;
            }

            return View(model);
        }

        [HttpPost]
        public string ListHistoryDetail(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var noreg = filter[0];
                    var nrm = filter[1];
                    IQueryable<FnB_ListPasienDetail_History> p = s.FnB_ListPasienDetail_History.Where(x => x.NRM == nrm);

                    var totalcount = p.Count();
                    var models = p.OrderByDescending(wkt => wkt.TanggalPenyajian).OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.DESC ? "ASC" : "DESC")}").ToArray();
                    result = new ResultSS(models.Length, null, totalcount, pageIndex);
                    var datas = new List<DetailHistoryPasienViewModel>();
                    var userId = User.Identity.GetUserId();
                    foreach (var x in models.ToList())
                    {
                        var m = IConverter.Cast<DetailHistoryPasienViewModel>(x);
                        m.TanggalPenyajian_View = x.TanggalPenyajian.ToString("yyyy/MM/dd");
                        datas.Add(m);
                    }

                    result.Data = datas;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}