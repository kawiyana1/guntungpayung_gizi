﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SystemFNB.Entities;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SystemFNB.Controllers
{
    public class HistoryPasienController : Controller
    {
        // GET: HistoryPasien
        public ActionResult Index()
        {
            return View();
        }

        #region ====== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<FnB_ListPasien_History> proses = s.FnB_ListPasien_History;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(FnB_ListPasien_History.NRM)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(FnB_ListPasien_History.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(FnB_ListPasien_History.NoIdentitas)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(FnB_ListPasien_History.Alamat)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(FnB_ListPasien_History.Phone)}.Contains(@0)", filter[6]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<HistoryPasienViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglLahir_View = x.TglLahir.Value.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}