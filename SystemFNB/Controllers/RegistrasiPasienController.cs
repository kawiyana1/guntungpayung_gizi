﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SystemFNB.Entities;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace SystemFNB.Controllers
{
    [Authorize(Roles = "FnB")]
    public class RegistrasiPasienController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        #region ===== T A B L E  R I
        [HttpPost]
        public string ListPasienRI(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                var datas = new List<RegistrasiPasienViewModel>();
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    if (filter[15] == "SEC003" || filter[15] == "SEC004")
                    {
                        IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg;
                        if (filter[24] != "True" && !string.IsNullOrEmpty(filter[23]) && !string.IsNullOrEmpty(filter[22]))
                        {
                            p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).Date);
                            p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]).Date);
                        }
                        if (filter[15] == "" || filter[15] == null)
                        {
                            p = p.Where($"{nameof(VW_DataPasienReg.SectionID)}.Contains(@0)", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                        }
                        else
                        {
                            p = p.Where($"{nameof(VW_DataPasienReg.SectionID)}.Contains(@0)", filter[15]);
                        }
                        if (filter[25] == "1")
                            p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                        else if (filter[25] == "2")
                            p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                        else if (filter[25] == "3")
                            p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                        else if (filter[25] == "4")
                            p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);

                        p = p.Where($"{nameof(VW_DataPasienReg.NoReg)}.Contains(@0)", filter[0]);
                        p = p.Where($"{nameof(VW_DataPasienReg.NRM)}.Contains(@0)", filter[6]);
                        p = p.Where($"{nameof(VW_DataPasienReg.NamaPasien)}.Contains(@0)", filter[7]);
                        p = p.Where($"{nameof(VW_DataPasienReg.JenisKerjasama)}.Contains(@0)", filter[10]);

                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                            .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                            m.Jam_View = x.Jam.ToString("HH\":\"mm");
                            m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                            m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                            m.EstimasiSisa_View = (x.EstimasiSisa == null ? "0" : x.EstimasiSisa.Value.ToMoney());
                            datas.Add(m);
                        }
                    }
                    else
                    {
                        IQueryable<Pelayanan_DataRegPasienRI> p = s.Pelayanan_DataRegPasienRI;
                        if (filter[24] != "True" && !string.IsNullOrEmpty(filter[23]) && !string.IsNullOrEmpty(filter[22]))
                        {
                            p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).Date);
                            p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]).Date);
                        }
                        if (filter[25] == "1")
                            p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.StatusBayar)}=@0 AND {nameof(Pelayanan_DataRegPasienRI.SudahPeriksa)}=@1", "Belum", false);
                        else if (filter[25] == "2")
                            p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.StatusBayar)}=@0 AND {nameof(Pelayanan_DataRegPasienRI.SudahPeriksa)}=@1", "Belum", true);
                        else if (filter[25] == "3")
                            p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.StatusBayar)}=@0 AND {nameof(Pelayanan_DataRegPasienRI.SudahPeriksa)}=@1", "Sudah Bayar", true);
                        else if (filter[25] == "4")
                            p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.StatusBayar)}=@0 AND {nameof(Pelayanan_DataRegPasienRI.SudahPeriksa)}=@1", "Sudah Bayar", false);
                        else if (filter[25] == "5")
                            p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.StatusBayar)}=@0", "Belum");
                        //if (!string.IsNullOrEmpty(filter[0]))
                        //    p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.NoAntri)} = @0", IFilter.F_short(filter[0]));
                        p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.NoReg)}.Contains(@0)", filter[0]);
                        p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.NRM)}.Contains(@0)", filter[6]);
                        p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.NamaPasien)}.Contains(@0)", filter[7]);
                        p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.JenisKerjasama)}.Contains(@0)", filter[10]);

                        if (!string.IsNullOrEmpty(filter[1])) p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.Kamar)}.Contains(@0)", filter[1]);
                        if (!string.IsNullOrEmpty(filter[2])) p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.NoBed)}.Contains(@0)", filter[2]);

                        if (filter[15] == "" || filter[15] == null)
                        {
                            p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.SectionID)}.Contains(@0)", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                        }
                        else
                        {
                            p = p.Where($"{nameof(Pelayanan_DataRegPasienRI.SectionID)}.Contains(@0)", filter[15]);
                        }
                        var totalcount = p.Count();
                        var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                            .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                        result = new ResultSS(models.Length, null, totalcount, pageIndex);
                        foreach (var x in models.ToList())
                        {
                            var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                            m.Jam_View = x.Jam == null ? "" : x.Jam.Value.ToString("HH\":\"mm");
                            m.Tanggal_View = x.Tanggal == null ? "" : x.Tanggal.Value.ToString("dd/MM/yyyy");
                            m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");

                            var kamar = s.mKamar.FirstOrDefault(e => e.NoKamar == m.Kamar);
                            if (kamar != null)
                            {
                                m.Kamar = kamar.NamaKamar;
                            }

                            var pasienkhusus = s.Vw_PasienBermasalah.OrderByDescending(z => z.TglInput).FirstOrDefault(z => z.NRM == x.NRM);
                            if (pasienkhusus != null)
                            {
                                m.Emoticon = pasienkhusus.Emoticon;
                            }

                            datas.Add(m);
                        }
                    }
                    
                    result.Data = datas;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region  ====== S E L E C T  2
        [HttpPost]
        public string ListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mDokter> proses = s.mDokter;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(mDokter.Active)}=@0", true);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<mDokter>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}