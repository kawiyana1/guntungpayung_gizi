//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SystemFNB.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class FnB_ListPasien_History
    {
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string NoIdentitas { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
    }
}
