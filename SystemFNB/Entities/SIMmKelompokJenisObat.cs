//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SystemFNB.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SIMmKelompokJenisObat
    {
        public string KelompokJenis { get; set; }
        public string Kelompok { get; set; }
        public Nullable<int> Grading { get; set; }
        public string KodeAwal { get; set; }
        public Nullable<double> DiscountKaryawan { get; set; }
        public Nullable<int> Akun_ID { get; set; }
        public Nullable<int> JenisPengadaanID { get; set; }
        public Nullable<int> Akun_ID_Mutasi { get; set; }
        public Nullable<bool> TidakPostingMutasi { get; set; }
        public Nullable<int> Akun_ID_Retur { get; set; }
    }
}
