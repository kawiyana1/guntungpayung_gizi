﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SystemFNB.Startup))]
namespace SystemFNB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
